<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Тестовое задание</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="css/styles.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="js/jquery.fancybox.min.js"></script>
	<script src="js/scripts.js"></script>
</head>
<body>
<?php
	require_once 'demo.php';
	use WineStyle\demo;
	$demo = new demo();
	echo $demo->output_all_thumbs();
	
	// Раскомментировать для вывода ошибки в атрибут alt изображения
//	echo $demo->output_thumb('test');
?>
</body>
</html>