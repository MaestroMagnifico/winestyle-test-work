<?php
// я использовал namespace чтобы выпендриться (класс generator вызывает конфликт имён)
namespace WineStyle;
setlocale(LC_ALL, 'ru_RU.utf8');

// debug
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

class generator {
	private $source_folder = './gallery/';
	private $target_folder = './cache/';
	private $font = './css/Montserrat-Medium.ttf';
	private static $error = '';
	
	function __construct() {
		// автоматом создаём директории для изображений, если они не существуют
		if (!file_exists($this->source_folder)) {
			mkdir($this->source_folder, 2775, true);
		}
		if (!file_exists($this->target_folder)) {
			mkdir($this->target_folder, 2775, true);
		}
	}
	
	/**
	 * Генерирует тестовые картинки (чтобы не качать самому)
	 */
	private function generate() {
		// генерируем уникальное имя для будущего файла
		$name_conflict = true;
		while ($name_conflict) {
			try {
				$name = bin2hex(random_bytes(5));
				if (!file_exists($this->source_folder.$name.'.jpg')) {
					$name_conflict = false;
				}
			} catch (\Exception $e) {
				echo 'Ошибка при генерации имени изображения';
			}
		}
		
		// создаём пустое изображение
		$image = imagecreatetruecolor(1920, 1080);
		// заливаем случайным цветом
		$bg_color = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));
		imagefill($image, 0, 0, $bg_color);
		
		// генерируем случайный цвет будущей надписи
		$text_color = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));
		// добавляем текст
		imagettftext($image, 90, -45, 500, 120, $text_color, $this->font, 'Тестовая картинка');
		
		// сохраняем изображение
		imagejpeg($image, $this->source_folder.$name.'.jpg', 100);
		// освобождаем память
		imagedestroy($image);
	}
	
	/**
	 * Генерирует 10 тестовых картинок
	 */
	public function generate10() {
		// удаляем старые изображения
		$files = scandir($this->source_folder);
		foreach ($files as $file) {
			if (substr_count(mb_strtolower($file), '.jpg')) {
				unlink($this->source_folder.$file);
			}
		}
		
		// удаляем миниатюры
		$files = scandir($this->target_folder);
		foreach ($files as $file) {
			if (substr_count(mb_strtolower($file), '.jpg')) {
				unlink($this->target_folder.$file);
			}
		}
		
		// генерируем 10 картинок
		for ($i = 1; $i <= 10; $i++) {
			$this->generate();
		}
		// перенаправляем юзера на страницу просмотра
		header("Location: /");
	}
	
	/**
	 * Создание миниатюры.
	 * Это дедушкин способ, в качестве примера. Лучше использовать ImageMagick.
	 * @param string $file_name - имя файла с картинкой без расширения
	 * @param string $size - big, med, min, mic
	 * @param bool $output - нужно ли возвращать картинку (для отладки)
	 */
	public function create_thumb(string $file_name, string $size, bool $output=true) {
		// поскольку $file_name по ТЗ у нас приходит без расширения, приписываем его
		$thumb_name = $file_name.'-'.$size.'.jpg';
		$file_name .= '.jpg';
		
		// проверяем существует ли исходник
		if (!file_exists($this->source_folder.$file_name)) {
			$this::$error = 'Исходное изображение не найдено.';
			return;
		}
		
		// проверяем создана ли уже миниатюра, если да, то просто выводим
		if (file_exists($this->target_folder.$thumb_name)) {
			if ($output) {
				$this->output_image($this->target_folder.$thumb_name);
			}
			return;
		}
		
		// вытаскиваем размеры из базы
		require_once 'db.php';
		$db = new db();
		$size = $db->escape($size);
		$query = $db->query("SELECT * FROM `image_sizes` WHERE `code`='$size'");
		$db->check_mysqli_error();
		$row = mysqli_fetch_array($query, MYSQLI_ASSOC);
		if (!$row['id']) {
			$this::$error = 'Размер типа '.$size.' не найден в базе.';
			return;
		}
		
		// получили ширину и высоту миниатюр
		$max_width = $row['width'];
		$max_height = $row['height'];
		
		// получаем размер оригинального изображения
		list($width, $height) = getimagesize($this->source_folder.$file_name);
		
		// против деления на ноль
		if (!$height) {
			$this::$error = 'Не удалось получить высоту исходного изображения.';
			return;
		}
		
		// определяем что больше не влезает - ширина или высота
		$width_diff = $width - $max_width;
		$height_diff = $height - $max_height;
		$aspect_ratio = $width / $height;
		
		if ($width_diff > $height_diff) {
			$crop_height = 0;
			$new_height = $max_height;
			$new_width = $new_height * $aspect_ratio;
			
			// находим на сколько не влезает ширина
			$crop_width = $new_width - $max_width;
			// делим на два - это размер смещения вправо
			$crop_width = 0 - $crop_width / 2;
			
			$new_width = $width / ($height / $max_height);
		} else {
			$crop_width = 0;
			$new_width = $max_width;
			$new_height = $new_width / $aspect_ratio;
			
			// находим на сколько не влезает высота
			$crop_height = $new_height - $max_height;
			// делим на два - это размер смещения вниз
			$crop_height = 0 - $crop_height / 2;
			
			$new_height = $height / ($width / $max_width);
		}
		
		// создаём миниатюру
		$image = imagecreatefromjpeg($this->source_folder.$file_name);
		$new_image = imagecreatetruecolor($max_width, $max_height);
		imagecopyresampled($new_image, $image, $crop_width, $crop_height, 0, 0, $new_width, $new_height, $width, $height);
		
		// сохраняем изображение
		imagejpeg($new_image,$this->target_folder.$thumb_name,100);
		// освобождаем память
		imagedestroy($new_image);
		
		if ($output) {
			// выводим изображение
			$this->output_image($this->target_folder.$thumb_name);
		}
	}
	
	/**
	 * Отдаёт изображение в браузер
	 * @param string $image_path
	 */
	private function output_image(string $image_path) {
		$image = file_get_contents($image_path);
		header('Content-type: image/jpeg;');
		header("Content-Length: " . strlen($image));
		echo $image;
	}
	
	/**
	 * Спагетти-код для отлавливания ошибок.
	 * Это можно было бы избежать, если бы в ТЗ не было условия, что SRC изображений должен обращаться напрямую к файлу generate.php
	 * @param string $file_name - имя файла с картинкой без расширения
	 * @param string $size - big, med, min, mic
	 * @return string
	 */
	public function check_errors(string $file_name, string $size):string {
		$this::$error = '';
		$this->create_thumb($file_name, $size, false);
		return $this::$error;
	}
}

// Исполняемая часть. По идеи, так делать не красиво, но в ТЗ написано, что всё должно быть в generate.php
if ($_GET['name'] and $_GET['size']) {
	$generator = new generator();
	$generator->create_thumb($_GET['name'], $_GET['size']);
}

// Для генерации исходников
if ($_GET['generate']) {
	$generator = new generator();
	$generator->generate10();
}