<?php
namespace WineStyle;
use mysqli;
use mysqli_result;

class db {
	private static $db_link;
	
	function __construct() {
		$this->connect_db();
	}
	
	/**
	 * Подключаемся к базе данных
	 * @return false|mysqli
	 */
	private function connect_db() {
		// не подключаемся дважды
		if ($this::$db_link) {
			return $this::$db_link;
		}
		
		require_once 'config.php';
		
		$db_link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		if (mysqli_connect_errno()) {
			die('Не удалось подключиться к базе данных: '. mysqli_connect_error());
		}
		if (!mysqli_set_charset($db_link, "utf8")) {
			die('Ошибка базы данных: '. mysqli_error($db_link));
		}
		$this::$db_link = $db_link;
		return $this::$db_link;
	}
	
	/**
	 * Обычная mysqli_query с упращённым синтаксисом
	 * @param $query
	 * @return bool|mysqli_result
	 */
	function query($query) {
		return mysqli_query($this::$db_link, $query);
	}
	
	/**
	 * Обычное экрнирование с упращённым синтаксисом
	 * @param $text - строка
	 * @return string
	 */
	function escape($text) {
		return mysqli_real_escape_string($this::$db_link, $text);
	}
	
	/**
	 * Проверка ошибки после выполения SQL запроса с выводом ошибки
	 * @param string $add - дополнительный текст ошибки
	 * @return bool
	 */
	function check_mysqli_error($add = '') {
		$error = mysqli_error($this::$db_link);
		if (!$error) {
			return false;
		}
		
		if ($add) {
			$error = '<b>'.$add.'</b>: '.$error;
		}
		
		die($error.': '. mysqli_error($this::$db_link));
	}
	
}